var isTouchDevice = function () {
    return 'ontouchstart' in window || 'onmsgesturechange' in window;
  },
  isDesktop = window.screenX != 0 || !isTouchDevice() ? true : false,
  isTriggerdBot = false;

(function () {
  "use strict";

  let vh = window.innerHeight * 0.01;
  // Then we set the value in the --vh custom property to the root of the document
  document.documentElement.style.setProperty('--vh', `${vh}px`);

  var quotes = document.querySelectorAll('.quote'),
    currentActive = 0;

  setInterval(function () {
    quotes.forEach(function (quote) {
      quote.className = 'quote';
    });

    currentActive += 1;
    if (currentActive > (quotes.length - 1)) {
      currentActive = 0;
    }
    quotes[currentActive].className = 'quote active';
  }, 4500);
})();

window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

function openContact (label) {
  "use strict";
  var input = document.getElementById('chat'),
    contact = document.getElementById('contact'),
    contactClassNamePersistance = contact.className;

  hideYoutube();
  ga('send', 'event', 'Contact', 'open', label);

  contact.addEventListener('click', function (e) {
    if (e.target !== this) {
      return;
    }
    contact.className = contactClassNamePersistance;
    ga('send', 'event', 'Contact', 'close', label);
  });

  contact.className = contactClassNamePersistance + ' active';

  input.focus();

  return false;
}

function handleSubmit () {
  "use strict";
  var input = document.getElementById('chat'),
    chatMsg = document.createElement('div'),
    chatContent = document.createElement('p'),
    chatMessages = document.getElementById('chat-messages'),
    persistedValue = input.value,
    msg1 = chatMsg.cloneNode(),
    msg2 = chatMsg.cloneNode(),
    p1 = chatContent.cloneNode(),
    p2 = chatContent.cloneNode(),
    matchValue = persistedValue.toLowerCase().replace('-', ''),
    previousMesageByYou = document.querySelector('.chat-msg:nth-last-child(2) p');

  msg1.className = 'chat-msg you';
  p1.innerHTML = input.value;
  msg1.appendChild(p1);
  chatMessages.appendChild(msg1);
  chatMessages.scrollTop = chatMessages.scrollHeight;
  input.value = '';
  input.focus();

  if (isDesktop) {
    if (isTriggerdBot && (matchValue === 'whatsapp' || matchValue === 'app')) {
      setTimeout(function () {
        msg2.className = 'chat-msg';
        p2.innerHTML = 'Oke, we openen WhatsApp voor je.';
        msg2.appendChild(p2);
        chatMessages.appendChild(msg2);
        chatMessages.scrollTop = chatMessages.scrollHeight;
        sendWhatsappAfterTimeout(input.getAttribute("data-phone"), persistedValue, 1500);
      }, 300);
    } else if (isTriggerdBot && (matchValue === 'email' || matchValue === 'mail')) {
      setTimeout(function () {
        msg2.className = 'chat-msg';
        p2.innerHTML = 'Oke, we openen de mail voor je.';
        msg2.appendChild(p2);
        chatMessages.appendChild(msg2);
        chatMessages.scrollTop = chatMessages.scrollHeight;
        setTimeout(function () {
          ga('send', 'event', 'Contact', 'sendMail', persistedValue);
          window.open('mailto:info@undercoverheroes.nl?body=' + persistedValue);
        }, 1500);
      }, 300);
    } else if (isTriggerdBot) {
      msg2.className = 'chat-msg';
      p2.innerHTML = 'Sorry, maar dat begreep ik even niet. Wil je nog een andere vraag stellen?';
      msg2.appendChild(p2);
      chatMessages.appendChild(msg2);
      chatMessages.scrollTop = chatMessages.scrollHeight;
      isTriggerdBot = false;
    } else {
      msg2.className = 'chat-msg';
      p2.innerHTML = 'We zien dat je chat vanaf een computer. Je kunt deze chat naar ons sturen via <a onclick="ga(\'send\', \'event\', \'Contact\', \'sendWhatsappClick\', \'' + persistedValue + '\');" href="https://wa.me/' + input.getAttribute("data-phone") + '?text=' + persistedValue + '" target="_blank">WhatsApp</a> of via <a onclick="ga(\'send\', \'event\', \'Contact\', \'sendMailClick\', \'' + persistedValue + '\');" href="mailto:info@undercoverheroes.nl?body=' + persistedValue + '">E-mail</a>. Wat heeft je voorkeur?';
      msg2.appendChild(p2);
      isTriggerdBot = true;
      setTimeout(function () {
        chatMessages.appendChild(msg2);
        chatMessages.scrollTop = chatMessages.scrollHeight;
      }, 300);
    }

  } else {
    sendWhatsappAfterTimeout(input.getAttribute("data-phone"), persistedValue, 500);
  }

  return false;
}

function sendWhatsappAfterTimeout (phone, msg, timeout) {
  setTimeout(function () {
    ga('send', 'event', 'Contact', 'sendWhatsapp', msg);
    window.open('https://wa.me/' + phone + '?text=' + msg, '_blank');
  }, timeout);
}

function handlePlay (e) {
  if (!isDesktop) {
    return true;
  }
  var youtubeOverlay = document.querySelector('.youtube-overlay');

  if (youtubeOverlay.className.indexOf('active') !== -1) {
    hideYoutube();
  } else {
    showYoutube();
  }

  return false;
}

function hideYoutube () {
  var youtubeOverlay = document.querySelector('.youtube-overlay'),
    youtubeEmbed = document.querySelector('.youtube-embed'),
    playIcon = document.querySelector('.fa-play'),
    pauseIcon = document.querySelector('.fa-pause');

  ga('send', 'event', 'Youtube', 'close', '');

  youtubeOverlay.className = 'youtube-overlay';
  youtubeEmbed.src = youtubeEmbed.src.replace('&autoplay=1', '');
  playIcon.style.display = 'inline-block';
  pauseIcon.style.display = 'none';
  setTimeout(function () {
    youtubeEmbed.style.display = 'none';
  }, 300);
}

function showYoutube () {
  var youtubeOverlay = document.querySelector('.youtube-overlay'),
    youtubeEmbed = document.querySelector('.youtube-embed'),
    playIcon = document.querySelector('.fa-play'),
    pauseIcon = document.querySelector('.fa-pause');

  ga('send', 'event', 'Youtube', 'open', '');

  youtubeOverlay.className = 'youtube-overlay active';
  youtubeEmbed.src = youtubeEmbed.src + '&autoplay=1';
  playIcon.style.display = 'none';
  pauseIcon.style.display = 'inline-block';
  youtubeEmbed.style.display = 'block';
}
